## To install
````
git clone https://gitlab.com/mirrorcell/react-starter.git
npm install -g yarn
yarn install
````


## To run in development mode
````
npm start -> Open browser at localhost:8888 (will make this automatic later)
````

## To build
````
npm run build
````

## To lint
````
npm run lint
````

## To test
TBA
