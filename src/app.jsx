import 'bootstrap/dist/css/bootstrap.min.css';
import styles from './styles/index.scss';
import React from 'react';
import GridTest from './js/components/Grid/grid-test';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <h1> Hello World </h1>
        <GridTest />
      </div>
    )
  }
}
