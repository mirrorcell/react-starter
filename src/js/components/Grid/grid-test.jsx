import React, {Component, PropTypes} from 'react';
import {Col, Row} from 'react-flexbox-grid';
import {autobind} from 'core-decorators';
import styles from './grid-test.scss';

console.log("STYLES", styles);

function GridItem({item, handleClick}) {
  return <Col xs onClick={handleClick.bind(null, item)} className={styles.gridTestColumn}> {item.value} </Col>
}

export default class GridTest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items : [],
    }
  }

  componentWillMount() {
    this.generateItems();
  }

  //Normally these would be passed down to this object through props
  @autobind
  generateItems() {
    const it = [];

    for(let i = 0; i < 1000; i++) {
      it.push({value: i, name: ''}) // Push dummy object
    }

    this.setState({
      items: it
    })
  }

  @autobind
  handleItemClick(item) {
    const {items} = this.state;
    const {value} = item;

    const index = items.findIndex(i => i.value === value)

    items.splice(index, 1);
    this.setState({items})

  }

  renderItems() {
    const {items} = this.state;
    return items.map(i => <GridItem key={i.value} item={i} handleClick={this.handleItemClick}/>)
  }

  render() {
    return (
      <div>
        Click an item to remove it
        <Row>
          {this.renderItems()}
        </Row>
      </div>
    )
  }
}
